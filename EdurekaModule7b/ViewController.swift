//
//  ViewController.swift
//  EdurekaModule7b
//
//  Created by Jay on 23/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    //@IBOutlet var nameOfHero: UILabel!
    @IBOutlet var basicAnimationLable: UILabel!
    
    //4 outlets for those things to show the completion stuff.
    @IBOutlet var basicAnimationLableA: UILabel!
    @IBOutlet var basicAnimationLableB: UILabel!
    @IBOutlet var basicAnimationLableC: UILabel!
    @IBOutlet var basicAnimationLableD: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //@IBAction func addNewItem(_ sender: UIButton)
    //this will fade the label
    @IBAction func basicAnimationFunction(_ sender: UIButton)
    {
        print("basicAnimationFunction called")
        
        basicAnimationLable.text="bye!"
        
        UIView.animate(withDuration: 0.5, animations:
            {
                self.basicAnimationLable.alpha = 0
            })
    }
    
    //this will fade them back.
    @IBAction func basicAnimationFunctionFadeBack(_ sender: UIButton)
    {
        print("basicAnimationFunctionFadeBack called")
        
        basicAnimationLable.text="hello!"
        
        UIView.animate(withDuration: 1,
                       animations:
                        {
                            self.basicAnimationLable.alpha = 1
                        },
                       completion:
                        { _ in
                            print("animation completed")
                            self.anotherAnimationFunction1()
                        })
    }
    
    func anotherAnimationFunction1()
    {
        print("anotherAnimationFunction1 called")
        
        UIView.animate(withDuration: 1,
                       animations:
            {
                self.basicAnimationLableA.alpha = 0
        },
                       completion:
            { _ in
                print("animation completed")
                self.anotherAnimationFunction2()
        })
    }
    
    func anotherAnimationFunction2()
    {
        print("anotherAnimationFunction2 called")
        
        UIView.animate(withDuration: 1,
                       animations:
            {
                self.basicAnimationLableB.alpha = 0
        },
                       completion:
            { _ in
                print("animation completed")
                self.anotherAnimationFunction3()
        })
    }
    
    func anotherAnimationFunction3()
    {
        print("anotherAnimationFunction3 called")
        
        UIView.animate(withDuration: 1,
                       animations:
            {
                self.basicAnimationLableC.alpha = 0
        },
                       completion:
            { _ in
                print("animation completed")
                self.anotherAnimationFunction4()
        })
    }
    
    func anotherAnimationFunction4()
    {
        print("anotherAnimationFunction4 called")
        
        UIView.animate(withDuration: 1,
                       animations:
            {
                self.basicAnimationLableD.alpha = 0
        },
                       completion:
            { _ in
                print("animation completed")
        })
    }
    
    //this will fade them back.
    @IBAction func basicAnimationFunctionCurve(_ sender: UIButton)
    {
        print("basicAnimationFunctionCurve called")
        
        basicAnimationLable.text="hello!"
        
        //setup the circle transform
        basicAnimationLable.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        //set the option of type of curve animation
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations:
            {
                self.basicAnimationLable.transform = .identity
            }, completion: nil)
    }
    
    @IBAction func basicAnimationFunctionCurveEaseIn(_ sender: UIButton)
    {
        print("basicAnimationFunctionCurveEaseIn called")
        
        basicAnimationLable.text="hello!"
        
        //setup the circle transform
        basicAnimationLable.transform = CGAffineTransform(scaleX: 25, y: 0)
        
        //set the option of type of curve animation
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseInOut, animations:
            {
                self.basicAnimationLable.transform = .identity
        }, completion: nil)
    }

}

